import Moon from 'moonjs';
// json config
import config from './config.json';
// styles
import './style.css';
// init moon
new Moon({
  el: '#root',
  data:{
    title: config.title
  }
});